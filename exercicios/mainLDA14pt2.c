#include <stdio.h>
#include <stdlib.h>



int main() {
	
	int numeros [5], i ;
	int * pnumeros;
	
	printf("escreva 5 numeros inteiros: \n");
	
	for(i=0 ;i < 5 ;i++) {
		scanf("%d", &numeros[i]);
	}
	// mostra a ordem digitada crescente usando ponteiro
	
	pnumeros = numeros;
		for (i = 0 ; i < 5; i++) {
		printf("%d \t", *pnumeros );
		pnumeros++;
		}
		printf("\n");
	// mostra a ordem digitada decrescente usando ponteiro
	
	*pnumeros = numeros[4];
	for(i=0 ; i < 5 ; i++){
		printf("%d \t" , *pnumeros);
		pnumeros--;
	}
	
}
